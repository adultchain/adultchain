### AdultChain is a fork of [PIVX](https://github.com/PIVX-Project/PIVX) that forked [Dash](https://github.com/dashpay/dash) that forked [Bitcoin](https://github.com/bitcoin/bitcoinp)


# AdultChain Core integration/staging repository


AdultChain is a cutting edge cryptocurrency, with many features not available in most other cryptocurrencies.
- Anonymized transactions using zerocoin technology.
- Fast transactions featuring guaranteed zero confirmation transactions, PIVX named it _SwiftX_.
- Decentralized blockchain voting providing for consensus based advancement of the current Masternode
  technology used to secure the network and provide the above features, each Masternode is secured
  with a collateral of 20K XXX.

More information at [AdultChain.me](https://adultchain.me) Visit our ANN thread at [BitcoinTalk](http://www.bitcointalk.org/index.php)


### Coin Specs
Algo Quark
Block Time	60 Seconds
Difficulty Retargeting	Every Block
Max Coin Supply	100,000,000 XXX
Premine	20,000,000 XXX



### Reward Distribution

PoW Phase

Block Height	Reward Amount	Notes	Duration (Days)
1	20,000,000 XXX	Initial Premine	0 Days
2-200	100 XXX	Open Mining	Approx 1 Day

PoS Phase

Block Height	Reward Amount
201-	Masternodes: 75%	Stakers: 25%

### PoW Rewards Breakdown

Block Height	Reward	Masternodes	Miner
2-200	100 XXX	0 XXX

### PoS Rewards Breakdown

Phase	Block Height	Reward	Masternodes	Stakers
Phase 1	201-135,000	100 XXX	75% (75 XXX)	25% (25 XXX)
Phase 2	135,001-390,000	80 XXX	75% (60 XXX)	25% (20 XXX)
Phase 3	390,001-800,000	50 XXX	75% (37.5 XXX)	25% (12.5 XXX)
Phase 4	800,001-1,500,000	20 XXX	75% (15 XXX)	25% (5 XXX)
Phase 5	1,500,001-2,660,026	10 XXX	75% (7.5 XXX)	25% (2.5 XXX)

AdultChain is a fork of PIVX that forked Dash that forked Bitcoin
AdultChain Core integration/staging repository
AdultChain is a cutting edge cryptocurrency, with many features not available in most other cryptocurrencies.

Anonymized transactions using zerocoin technology.
Fast transactions featuring guaranteed zero confirmation transactions, PIVX named it SwiftX.
Decentralized blockchain voting providing for consensus based advancement of the current Masternode technology used to secure the network and provide the above features, each Masternode is secured with a collateral of 20K XXX.
More information at AdultChain.me Visit our ANN thread at BitcoinTalk